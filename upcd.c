/* Name: upcd.c
 * Project: UPCD
 * Author: Maximilian Niedernhuber
 * Creation Date: 2020-04-26
 * Copyright: (c) 2021 draketronic.de
 * License:  GNU GPL v3
 */
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <argp.h>
#include <confuse.h>
#include <libusb-1.0/libusb.h>
#include <curl/curl.h>


#define  USB_CFG_VENDOR_ID       0xc0, 0x16
#define  USB_CFG_DEVICE_ID       0xdc, 0x05 /* = 0x05dc = 1500 */
#define  USB_CFG_VENDOR_NAME     'd', 'r', 'a', 'k', 'e', 't', 'r', 'o', 'n', 'i', 'c'
#define  USB_CFG_DEVICE_NAME     'U', 'P', 'C'


const uint8_t chMap[10] = {0,1,2,3,4,9,8,7,6,5};

//CLI parser beginn
const char *argp_program_version = "UPCD V1.0";
static char doc[] = "UPCD -- Daemon for Universal Pulse Counter";
const char *argp_program_bug_address = "<m.niedernhuber@draketronic.de>";

/* The options we understand. */
static struct argp_option options[] = {
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"config",   'c', "CONFIG_FILE", 0,  "Configuration file, Default: upcd.cfg" },
  { 0 }
};

struct arguments
{
  char *args[2];                /* arg1 & arg2 */
  int  verbose;
  char *config_file;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;

  switch (key)
    {
    case 'v':
      arguments->verbose = 1;
      break;
    case 'c':
      arguments->config_file = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, 0, doc };

//CLI parser end

//Config parser begin
cfg_opt_t entry[] = {
    CFG_INT("channel", -1,CFGF_NONE),
    CFG_STR("link", NULL, CFGF_NONE),
    CFG_INT("updateCycle", -1,CFGF_NONE),
    CFG_INT("pulsePerKUnitHour", -1,CFGF_NONE),
    CFG_END()
};

cfg_opt_t opts[] = {
	CFG_SEC("entry", entry, CFGF_MULTI | CFGF_TITLE),
	CFG_END()
};
//Config parser end

typedef struct {
    int32_t counter;
    int64_t lastPulseOccured;
    int64_t lastUpdateOccured;
    int32_t updateCounter;
} entryWorker_t;

int main(int argc, char **argv)
{
    //Vars for CLI arguments
    struct arguments arguments;

    //Vars for config file
    cfg_t *cfg;
    int entryCount;
    entryWorker_t *entryWorker;

    //Vars for CURL
    CURL *curl;
    CURLcode res;

    //Vars for USB
    libusb_device_handle    *handle = NULL;
    const unsigned char     rawVid[2] = {USB_CFG_VENDOR_ID}, rawPid[2] = {USB_CFG_DEVICE_ID};

    uint8_t                 buffer[10];
    int                     cnt, vid, pid;

    //Init with default arguments
    arguments.verbose = 0;
    arguments.config_file = "upcd.cfg";

    argp_parse (&argp, argc, argv, 0, 0, &arguments);

    if(arguments.verbose == 1){
        fprintf(stderr, "Verbose enabled\n");
        fprintf(stderr, "Load configfile: %s\n", arguments.config_file);
    }

    cfg = cfg_init(opts, CFGF_NOCASE);
    cfg_parse(cfg, arguments.config_file);
    entryCount = cfg_size(cfg, "entry");
    entryWorker = malloc(sizeof(entryWorker_t)*entryCount);

    //Init worker memory
    for(int i=0;i<entryCount;i++){
        entryWorker[i].counter = 0;
        entryWorker[i].updateCounter = -1;
        entryWorker[i].lastPulseOccured = -1;
        entryWorker[i].lastUpdateOccured = -1;
    }

    if(arguments.verbose == 1){
        fprintf(stderr, "%i entry loaded from configfile\n", entryCount);
    }

    libusb_init(0);

    /* compute VID/PID from usbconfig.h so that there is a central source of information */
    vid = rawVid[1] * 256 + rawVid[0];
    pid = rawPid[1] * 256 + rawPid[0];

    handle = libusb_open_device_with_vid_pid(0,vid,pid);

    if(handle == 0){
        fprintf(stderr, "Could not find USB device with vid=0x%x pid=0x%x\n", vid, pid);
        exit(1);
    }

    curl = curl_easy_init();

    while(1){
        struct timespec ts;
        cnt = libusb_control_transfer(handle, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN, 0, 0, 0, buffer, sizeof(buffer), 500);
        clock_gettime(CLOCK_MONOTONIC, &ts);

        if(cnt < 10){
            if(cnt < 0){
                fprintf(stderr, "USB error: %s\n", libusb_strerror (cnt));

                if(cnt == LIBUSB_ERROR_NO_DEVICE){
                    break;
                }

            }else{
                fprintf(stderr, "only %d bytes received.\n", cnt);
            }
        }else{
            if(arguments.verbose == 1){
                fprintf(stderr, "Received data: %i, %i, %i, %i, %i, %i, %i, %i, %i, %i\n", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[9], buffer[8], buffer[7], buffer[6], buffer[5]);
            }

            for(int i=0;i<entryCount;i++){
                cfg_t *ecfg = cfg_getnsec(cfg, "entry", i);
                int32_t channel = cfg_getint(ecfg,"channel");
                int32_t pulsePerKUnitHour = cfg_getint(ecfg,"pulsePerKUnitHour");
                bool sendData = false;
                uint32_t data = 0;

                if(channel > sizeof(buffer)){
                    fprintf(stderr, "Specified channel %i is out of range", channel);
                    continue;
                }

                //Map Channel to the transferred array index
                channel = chMap[channel-1];

                if(buffer[channel] != 0 && entryWorker[i].updateCounter != -1){

                    //Add to Worker Counter
                    entryWorker[i].counter += buffer[channel];

                    //Store Timestamp
                    entryWorker[i].lastPulseOccured = ts.tv_sec*1000LL + ts.tv_nsec/1000000;
                }

                if(entryWorker[i].updateCounter > 0){
                    entryWorker[i].updateCounter--;
                }
                else if( entryWorker[i].updateCounter < 0 ){
                    entryWorker[i].updateCounter = cfg_getint(ecfg,"updateCycle");
                }
                else if( entryWorker[i].updateCounter == 0 ){

                    //Do pulse to unit convertion
                    if(pulsePerKUnitHour > 0 && entryWorker[i].lastUpdateOccured != -1){
                        uint32_t time_ms;
                        //TS_Diff
                        if(entryWorker[i].counter == 0){
                            data = 0;
                        }else{
                            time_ms = (ts.tv_sec*1000LL + ts.tv_nsec/1000000)-entryWorker[i].lastUpdateOccured;
                            time_ms /= entryWorker[i].counter;
                            data = 3600000000UL;
                            data = data/(pulsePerKUnitHour*time_ms);
                        }
                        sendData = true;
                    }
                    else if(pulsePerKUnitHour <= 0){ //Pulse Count only
                        data = entryWorker[i].counter;
                        sendData = true;
                    }

                    //Store Timestamp
                    entryWorker[i].lastUpdateOccured = ts.tv_sec*1000LL + ts.tv_nsec/1000000;

                    //Reload Counter
                    entryWorker[i].updateCounter = cfg_getint(ecfg,"updateCycle");
                    entryWorker[i].counter = 0;
                }


                //Send data
                if(curl && sendData) {
                    char url[1024];

                    snprintf(url,1024,(const char*)cfg_getstr(ecfg,"link"),data);

                    if(arguments.verbose == 1){
                        fprintf(stderr, "Updating entry %s with data %i\n", cfg_title(ecfg), data);
                        fprintf(stderr, "URL: %s\n", url);
                    }

                    curl_easy_setopt(curl, CURLOPT_URL, url );
                    /* example.com is redirected, so we tell libcurl to follow redirection */
                    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

                    /* Perform the request, res will get the return code */
                    res = curl_easy_perform(curl);
                    /* Check for errors */
                    if(res != CURLE_OK){
                      fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
                    }
                  }

            }

        }
        sleep(1);
    }

    curl_easy_cleanup(curl);
    libusb_close(handle);
    libusb_exit(0);
    cfg_free(cfg);
    free(entryWorker);
    exit (0);
    return 0;
}
