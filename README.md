# Project description
UPCD is a daemon for the USB Pulse Counter from draketronic to interface it with a logging application like [emonCMS](Emoncms.org). The USB Pulse Counter (UPC) can monitor upto 10 S0 ports of e.g. electricity meters. The hardware can be brought on [draketronic.de](http://www.draketronic.de).
The UPCD can also measuring the time between pulses to get a instantaneous value e.g. power in W.



# Build UPCD
Install dependencies:
`apt-get install libconfuse-dev libusb-1.0-0-dev libcurl4-openssl-dev`

Build binary:
`gcc upcd.c -lusb-1.0 -lcurl -lconfuse -o upcd`

# Install UPCD
sudo cp upcd /usr/bin/

# Create Configuration
see upcd_example.cfg for upcd_example  
Options for each entry 
| option | description |
| ------ | ------ |
| channel | input port |
| link | Link to post data e.g. emoncms |
| updateCycle | Update cycle time in seconds or timeout for instantaneous mode |
| pulsePerKUnitHour | enable instantaneous mode with calculation based on this value |

# Start UPCD
upcd -c pathToCfg
